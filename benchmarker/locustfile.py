from locust import HttpLocust, TaskSet, task


class MyTaskSet(TaskSet):

    @task(1)
    def root(self):
        self.client.get('/')

    @task(1)
    def style(self):
        self.client.get('/static/style.css')

    @task(1)
    def entliczek(self):
        self.client.get('/search?q=entliczek')

    @task(1)
    def pentliczek(self):
        self.client.get('/search?q=pentliczek')

    @task(1)
    def czerwony(self):
        self.client.get('/search?q=czerwony')

    @task(1)
    def stoliczek(self):
        self.client.get('/search?q=stoliczek')


class MyLocust(HttpLocust):
    task_set = MyTaskSet
    min_wait = 100
    max_wait = 1000

