drop table if exists entries;
create table entries (
    id integer primary key autoincrement,
    username string not null,
    query string not null,
    datetime string not null
);